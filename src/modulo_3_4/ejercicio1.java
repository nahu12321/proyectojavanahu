package modulo_3_4;

import java.util.Scanner;

public class ejercicio1 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		double nota1, nota2, nota3, promedio;
		
		System.out.println("Primer nota --> ");
		nota1 = scan.nextDouble();
		System.out.println("Segunda nota --> ");
		nota2 = scan.nextDouble();
		System.out.println("Tercer nota --> ");
		nota3 = scan.nextDouble();
		
		promedio=(nota1+nota2+nota3)/3;
		
		if(promedio>=7) {
			System.out.println("Aprobaste con --> "+promedio);
		}
		else {
			System.out.println("Desaprobaste con --> "+promedio);
		}
		
		scan.close();

	}

}
